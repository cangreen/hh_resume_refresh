<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_resume".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $resume_id
 * @property int $auto
 * @property User $user
 */
class Resume extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resume';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'auto'], 'integer'],
            [['auto'], 'integer'],
            ['auto', 'default', 'value' => 1],
            [['resume_id'], 'string', 'max' => 255],
            [['resume_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resume_id' => 'Resume ID',
            'auto' => 'Auto',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
