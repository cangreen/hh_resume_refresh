<?php

namespace app\controllers;

use app\models\HhData;
use app\models\LoginForm;
use app\models\Resume;
use app\models\SignupForm;
use app\models\User;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\httpclient\Client;
use Yii;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'publish' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $user = Yii::$app->user;
        $userModel = $user->identity;

        if ($user->isGuest) {
            return $this->redirect('login');
        }

        if (is_null($userModel->hh_access_token) || $userModel->hh_access_token_expire < strtotime(date('Y-m-d H:i:s'))) {
            Yii::$app->session->setFlash('isTokenExist', 'Токен пользователя отсутствует или просрочен. Обновите токен');
        }

        return $this->render('index');
    }

    /**
     * @param $code
     * @return void|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionAuth($code)
    {
        $user = Yii::$app->user->identity;
        /** @var $user User */
        if ($code && is_null($user->hh_access_token)) {
            $user->setAttribute('hh_access_token', $code);
        }
        if ($user->save()) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl('https://hh.ru/oauth/token')
                ->setData([
                    'grant_type' => 'authorization_code',
                    'client_id' => Yii::$app->hh->client_id,
                    'client_secret' => Yii::$app->hh->client_secret,
                    'redirect_uri' => Yii::$app->hh->redirect_uri,
                    'code' => $code,
                ])
                ->send();
            if ($response->isOk) {
                Yii::$app->session->setFlash('isTokenExist', 'Токен успешно получен');
                $user->hh_access_token = $response->data['access_token'];
                $user->hh_refresh_token = $response->data['refresh_token'];
                $user->hh_access_token_expire = strtotime(date('Y-m-d H:i:s')) + $response->data['expires_in'];
            } else {
                Yii::$app->session->setFlash('isTokenExist', "{$response->data['error']} <br> {$response->data['error_description']} ");
            }
            if ($user->save()) {
                return $this->redirect('/');
            }
        }
    }

    public function actionResumes()
    {
        $user = Yii::$app->user->identity;
        /** @var $user User */

        $client = new Client();
        try {
            $response = $client->createRequest()
                ->setMethod('GET')
                ->setUrl('https://api.hh.ru/resumes/mine')
                ->setHeaders([
                    'Authorization' => 'Bearer ' . $user->hh_access_token,
                    'User-Agent' => 'api-test-agent'
                ])
                ->send();
            if ($response->isOk) {
                $items = $response->data['items'];
                $this->checkResumes($items);
                return $this->render('resumes', [
                    'resumes' => $this->getResumesWithAuto($items)
                ]);
            }

        } catch (\Exception $exception) {
            Yii::error($exception->getMessage());
            Yii::$app->session->setFlash('resumesError', "
                {$response->data['description']} <br>
                {$response->data['errors'][0]['value']} <br>
                {$response->data['errors'][0]['type']} <br>
        "
            );
            return $this->render('resumes');
        }
    }

    public function actionPublish($id)
    {
        $user = Yii::$app->user->identity;
        /** @var $user User */

        $client = new Client();

        try {
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl("https://api.hh.ru/resumes/{$id}/publish")
                ->setHeaders([
                    'Authorization' => 'Bearer ' . $user->hh_access_token,
                    'User-Agent' => 'api-test-agent'
                ])
                ->send();
            if ($response->isOk) {
                return $this->redirect('resumes');
            }
        } catch (\Exception $exception) {
            Yii::error($exception->getMessage());
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionRegister()
    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @param array $items
     * @return void
     */
    private function checkResumes(array $items): void
    {
        foreach ($items as $item) {
            if (!Resume::findOne(['resume_id' => $item['id']])) {
                $model = new Resume();
                $model->setAttribute('resume_id', $item['id']);
                $model->setAttribute('user_id', Yii::$app->user->identity->id);
                try {
                    $model->save();
                } catch (\Exception $exception) {
                    Yii::error($exception->getMessage());
                }
            }
        }
    }

    /**
     * @param array $items
     * @return array
     */
    private function getResumesWithAuto(array $items): array
    {
        $result = [];

        foreach ($items as $item) {
            $item['auto'] = Resume::findOne(['resume_id' => $item['id']])->auto;
            $result[] = $item;
        }

        return $result;
    }

    public function actionResumeAuto()
    {
        if ($post = \Yii::$app->request->post()) {
            $model = Resume::findOne(['resume_id' => $post['id']]);
            $model->setAttribute('auto', (int)$post['auto']);
            try {
                Yii::$app->response->data = Json::encode(['status' => 'ok', 'errors' => $model->save() ? '' : $model->errors]);
            } catch (\Exception $exception) {
                Yii::error($exception->getMessage());
            }
        }
    }
}
