<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_resume}}`.
 */
class m230425_122850_create_user_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%resume}}', [
            'id' => $this->primaryKey(),
            'resume_id' => $this->string()->unique(),
            'user_id' => $this->integer()->notNull(),
            'auto' => $this->boolean()->notNull()->defaultValue(1)
        ]);

        $this->addForeignKey('resume_fk1', 'resume', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropForeignKey('resume_fk1', 'resume');

        $this->dropTable('{{%resume}}');
    }
}
