<?php


namespace app\commands;

use app\models\HhData;
use app\models\Resume;
use yii\console\Controller;
use yii\httpclient\Client;

/**
 *  hh.ru resume refresh
 * @package app\commands
 */
class RefreshController extends Controller
{
    public function actionIndex($id = null)
    {
        $models = [];

        if (is_null($id)) {
            $models = Resume::find()->where(['auto' => 1])->all();
        } else {
            $models[] = Resume::findOne([
                'resume_id' => $id,
                'auto' => 1
            ]);
        }

        if (!empty($models)) {
            foreach ($models as $model) {
                /** @var $model Resume */

                try {
                    $client = new Client();
                    $client->createRequest()
                        ->setMethod('POST')
                        ->setUrl("https://api.hh.ru/resumes/{$model->resume_id}/publish")
                        ->setHeaders([
                            'Authorization' => 'Bearer ' . $model->user->hh_access_token,
                            'User-Agent' => 'api-test-agent'
                        ])
                        ->send();
                } catch (\Exception $e) {
                    \Yii::error($e->getMessage());
                }
            }
        }
    }
}