$('input[type="checkbox"]').change(function () {
    // $('input[type="checkbox"]').attr('disabled', true);
    // console.log(this.disabled);
    $.post('/resume-auto',
        {
            id: this.dataset.id,
            'auto': this.checked ? 1 : 0
        }, function (data) {
            if (JSON.parse(data).errors === '') {
                location.reload();
            }
        });
});